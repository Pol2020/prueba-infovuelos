<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Add a line to a map using a GeoJSON source</title>
<meta name="viewport" content="initial-scale=1,maximum-scale=1,user-scalable=no">
<link href="https://api.mapbox.com/mapbox-gl-js/v2.10.0/mapbox-gl.css" rel="stylesheet">
<script src="https://api.mapbox.com/mapbox-gl-js/v2.10.0/mapbox-gl.js"></script>
<style>
body { margin: 0; padding: 0; }
#map { position: absolute; top: 0; bottom: 0; width: 100%; }
</style>
</head>

<body>
<div id="map"></div>

<script>

	// https://account.mapbox.com
	mapboxgl.accessToken = 'pk.eyJ1IjoiY2FybG9jb3Jkb3ZhMjEiLCJhIjoiY2t0MGJqZnR6MDNubTJvbm1kbzViY2twdyJ9.rxfdc1n_MQsYqdGzxRJx-g';

const map = new mapboxgl.Map({
container: 'map',
// Choose from Mapbox's core styles, or make your own style with Mapbox Studio
//Este estilo muestra las calles a blanco y negro: style: 'mapbox://styles/mapbox/light-v10',
//center: [-77.035, 38.875],
style: 'mapbox://styles/mapbox/streets-v11', // Este estilo muestra las calles a color
center: [-122.486052, 37.830348],
zoom: 12

});

const geojson = {
'type': 'FeatureCollection',
'features': [
{
'type': 'Feature',
'properties': {},
'geometry': {
'coordinates': [
    /*
    [-77.044211, 38.852924],
[-77.045659, 38.860158],
[-77.044232, 38.862326],
[-77.040879, 38.865454],
[-77.039936, 38.867698],
[-77.040338, 38.86943],
[-77.04264, 38.872528],
[-77.03696, 38.878424],
[-77.032309, 38.87937],
[-77.030056, 38.880945],
[-77.027645, 38.881779],
[-77.026946, 38.882645],
[-77.026942, 38.885502],
[-77.028054, 38.887449],
[-77.02806, 38.892088],
[-77.03364, 38.892108],
[-77.033643, 38.899926]
*/

[-122.483696, 37.833818],
[-122.483482, 37.833174],
[-122.483396, 37.8327],
[-122.483568, 37.832056],
[-122.48404, 37.831141],
[-122.48404, 37.830497],
[-122.483482, 37.82992],
[-122.483568, 37.829548],
[-122.48507, 37.829446],
[-122.4861, 37.828802],
[-122.486958, 37.82931],
[-122.487001, 37.830802],
[-122.487516, 37.831683],
[-122.488031, 37.832158],
[-122.488889, 37.832971],
[-122.489876, 37.832632],
[-122.490434, 37.832937],
[-122.49125, 37.832429],
[-122.491636, 37.832564],
[-122.492237, 37.833378],
[-122.493782, 37.833683]

],
'type': 'LineString'
}
}
]
};

 
map.on('load', () => {
    /*
map.addSource('route', {
'type': 'geojson',
'data': {
'type': 'Feature',
'properties': {},
'geometry': {
'type': 'LineString',
'coordinates': [
[-122.483696, 37.833818],
[-122.483482, 37.833174],
[-122.483396, 37.8327],
[-122.483568, 37.832056],
[-122.48404, 37.831141],
[-122.48404, 37.830497],
[-122.483482, 37.82992],
[-122.483568, 37.829548],
[-122.48507, 37.829446],
[-122.4861, 37.828802],
[-122.486958, 37.82931],
[-122.487001, 37.830802],
[-122.487516, 37.831683],
[-122.488031, 37.832158],
[-122.488889, 37.832971],
[-122.489876, 37.832632],
[-122.490434, 37.832937],
[-122.49125, 37.832429],
[-122.491636, 37.832564],
[-122.492237, 37.833378],
[-122.493782, 37.833683]
]
}
}

});
*/

map.addSource('line', {
type: 'geojson',
lineMetrics: true,
data: geojson
});
/*
map.addLayer({
'id': 'route',
'type': 'line',
'source': 'route',
'layout': {
'line-join': 'round',
'line-cap': 'round'
},
'paint': {
'line-color': '#888',
'line-width': 8
}
});
*/

map.addLayer({
type: 'line',
source: 'line',
id: 'line',
paint: {
'line-color': 'red',
'line-width': 14,
// 'line-gradient' must be specified using an expression
// with the special 'line-progress' property
'line-gradient': [
'interpolate',
['linear'],
['line-progress'],
0,
'blue',
0.1,
'royalblue',
0.3,
'cyan',
0.5,
'lime',
0.7,
'yellow',
1,
'red'
]
},
layout: {
'line-cap': 'round',
'line-join': 'round'
}
});

});
</script>
 
</body>
</html>